# Git Assignment

Answser the following questions. You must commit each anwser.

Q1) Which SCM [^1] functions do *git* + *GitLab* implement?

[^1]: SCM stands for Software Configurartion Management, which is a Software Configuration (SQA) component.

- [x] Version Control
- [ ] Change control
- [ ] Code tracking
- [ ] All the above
- [ ] None of the above

Q2) Which of the following is not *version control* function?
 
- [ ] Rastreamento de alterações no código
- [x] Integração de código
- [ ] Automatização de testes
- [ ] Análise de incongruências nas versões do código
- [ ] Nenhuma das opções apresentadas

By Cristóvão Sousa